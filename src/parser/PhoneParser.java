package parser;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneParser {
  public String parseFromFile(String filename) throws FileNotFoundException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream(filename);
    Scanner scanner = new Scanner(stream);

    String regexPhoneNumber = "\\+\\d\\(\\d{3}\\)\\s\\d{3}\\s\\d{2}\\s\\d{2}";
    Pattern pattern = Pattern.compile(regexPhoneNumber);
    Matcher matcher;

    StringBuffer foundPhones = new StringBuffer();
    while (scanner.hasNextLine()) {
      matcher = pattern.matcher(scanner.nextLine());
      while (matcher.find())
        foundPhones.append(matcher.group()).append("r");
    }

    String regexExcess = "[()+\\s]?";
    pattern = Pattern.compile(regexExcess);

    matcher = pattern.matcher(foundPhones);
    StringBuffer inputString = new StringBuffer();
    while (matcher.find())
      matcher.appendReplacement(inputString, "");

    String regexReturn = "[r]";
    pattern = Pattern.compile(regexReturn);

    matcher = pattern.matcher(inputString);
    inputString = new StringBuffer();
    while (matcher.find())
      matcher.appendReplacement(inputString, "\n");

    return inputString.toString();
  }
}
