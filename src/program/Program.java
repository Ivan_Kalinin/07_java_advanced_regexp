package program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import parser.PhoneParser;

public class Program {

  public static void main(String[] args) {
    String inputFile = "resources/input.txt";
    String outputFile = "src/resources/output.txt";

    PhoneParser phoneParser = new PhoneParser();
    try {
      String result = phoneParser.parseFromFile(inputFile);

      File file = new File(outputFile);
      if (file.exists()) {
        FileWriter writer = new FileWriter(file);
        writer.write(result);
        writer.close();
        System.out.println("complete");
      } else {
        System.out.println("not wrote");
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
